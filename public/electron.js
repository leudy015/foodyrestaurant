const electron = require("electron");
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const path = require("path");
const isDev = require("electron-is-dev");
const Pushy = require("pushy-electron");

let mainWindow;
function createWindow() {
  mainWindow = new BrowserWindow({
    minHeight: 800,
    minWidth: 1300,
    width: 1300,
    height: 800,
    darkTheme: true,
    title: "Foody for Restaurant",
    titleBarStyle: "hiddenInset",
  });
  mainWindow.loadURL(
    isDev
      ? "http://localhost:3000"
      : `file://${path.join(__dirname, "../build/index.html")}`
  );

  mainWindow.webContents.on("did-finish-load", () => {
    // Initialize Pushy
    Pushy.listen();

    // Register device for push notifications
    Pushy.register({ appId: "5f4e39df3ed983523ca061d9" })
      .then((deviceToken) => {
        // Display an alert with device token
        //Pushy.alert(win, "Pushy device token: " + deviceToken);
      })
      .catch((err) => {
        // Display error dialog
        Pushy.alert(mainWindow, "Pushy registration error: " + err.message);
      });

    // Listen for push notifications
    Pushy.setNotificationListener((data) => {
      // Display an alert with the "message" payload value
      Pushy.alert(mainWindow, data.message);
    });
  });

  if (isDev) {
    // Open the DevTools.
    //BrowserWindow.addDevToolsExtension('<location to your react chrome extension>');
    // mainWindow.webContents.openDevTools();
  }
  mainWindow.on("closed", () => (mainWindow = null));
}
app.on("ready", createWindow);
app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});
app.on("activate", () => {
  if (mainWindow === null) {
    createWindow();
  }
});
