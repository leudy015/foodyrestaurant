import gql from "graphql-tag";

const RESTAURANT = gql`
  query getRestaurantForID($id: ID) {
    getRestaurantForID(id: $id) {
      messages
      success
      data {
        _id
        title
        image
        description
        rating
        anadidoFavorito
        type
        address {
          calle
          numero
          codigoPostal
          ciudad
        }
        Valoracion {
          id
          comment
          value
          created_at
        }
        coordetate
        coordenateone
        categoryID
        categoryName
        minime
        menu
        city
        phone
        email
        isnew
        logo
        apertura
        cierre
        diaslaborales
        open
        OnesignalID
      }
    }
  }
`;

const GET_ORDEN = gql`
  query getOrderByRestaurant($id: ID, $dateRange: DateRangeInput) {
    getOrderByRestaurant(id: $id, dateRange: $dateRange) {
      message
      success
      list {
        id
        nota
        isvalored
        aceptaTerminos
        usuario {
          _id
          name
          lastName
          email
          avatar
          city
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          OnesignalID
        }
        restaurant
        restaurants {
          _id
          title
          image
          description
          type
          rating
          address {
            calle
            ciudad
            codigoPostal
            numero
          }
          coordetate
          anadidoFavorito
          coordenateone
          categoryName
          categoryID
          minime
          city
          phone
          email
          logo
          password
          apertura
          cierre
          diaslaborales
          open
          OnesignalID
        }
        time
        cantidad
        userID
        propina
        platos {
          userId
          platoID
          restaurant
          complementos
          plato {
            _id
            title
            ingredientes
            price
            imagen
            restaurant
            menu
            oferta
            popular
            anadidoCart
            cant
          }
        }
        cubiertos
        estado
        status
        progreso
        created_at
        total
      }
    }
  }
`;

const GET_ORDEN_NEW = gql`
  query getOrderByRestaurantNew($id: ID, $dateRange: DateRangeInput) {
    getOrderByRestaurantNew(id: $id, dateRange: $dateRange) {
      message
      success
      list {
        id
        nota
        isvalored
        aceptaTerminos
        usuario {
          _id
          name
          lastName
          email
          avatar
          city
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          OnesignalID
        }
        restaurant
        restaurants {
          _id
          title
          image
          description
          type
          rating
          address {
            calle
            ciudad
            codigoPostal
            numero
          }
          coordetate
          anadidoFavorito
          coordenateone
          categoryName
          categoryID
          minime
          city
          phone
          email
          logo
          password
          apertura
          cierre
          diaslaborales
          open
          OnesignalID
        }
        time
        cantidad
        userID
        propina
        platos {
          userId
          platoID
          restaurant
          complementos
          plato {
            _id
            title
            ingredientes
            price
            imagen
            restaurant
            menu
            oferta
            popular
            anadidoCart
            cant
          }
        }
        cubiertos
        estado
        status
        progreso
        created_at
        total
      }
    }
  }
`;

const GET_ORDEN_PROCCESS = gql`
  query getOrderByRestaurantProcess($id: ID, $dateRange: DateRangeInput) {
    getOrderByRestaurantProcess(id: $id, dateRange: $dateRange) {
      message
      success
      list {
        id
        nota
        isvalored
        aceptaTerminos
        usuario {
          _id
          name
          lastName
          email
          avatar
          city
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          OnesignalID
        }
        restaurant
        restaurants {
          _id
          title
          image
          description
          type
          rating
          address {
            calle
            ciudad
            codigoPostal
            numero
          }
          coordetate
          anadidoFavorito
          coordenateone
          categoryName
          categoryID
          minime
          city
          phone
          email
          logo
          password
          apertura
          cierre
          diaslaborales
          open
          OnesignalID
        }
        time
        cantidad
        userID
        propina
        platos {
          userId
          platoID
          restaurant
          complementos
          plato {
            _id
            title
            ingredientes
            price
            imagen
            restaurant
            menu
            oferta
            popular
            anadidoCart
            cant
          }
        }
        cubiertos
        estado
        status
        progreso
        created_at
        total
      }
    }
  }
`;

const GET_ORDEN_FINISH = gql`
  query getOrderByRestaurantListos($id: ID, $dateRange: DateRangeInput) {
    getOrderByRestaurantListos(id: $id, dateRange: $dateRange) {
      message
      success
      list {
        id
        nota
        isvalored
        aceptaTerminos
        usuario {
          _id
          name
          lastName
          email
          avatar
          city
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          OnesignalID
        }
        restaurant
        restaurants {
          _id
          title
          image
          description
          type
          rating
          address {
            calle
            ciudad
            codigoPostal
            numero
          }
          coordetate
          anadidoFavorito
          coordenateone
          categoryName
          categoryID
          minime
          city
          phone
          email
          logo
          password
          apertura
          cierre
          diaslaborales
          open
          OnesignalID
        }
        time
        cantidad
        userID
        propina
        platos {
          userId
          platoID
          restaurant
          complementos
          plato {
            _id
            title
            ingredientes
            price
            imagen
            restaurant
            menu
            oferta
            popular
            anadidoCart
            cant
          }
        }
        cubiertos
        estado
        status
        progreso
        created_at
        total
      }
    }
  }
`;

const GET_ORDEN_ID = gql`
  query getOrderByRestaurantID($id: ID) {
    getOrderByRestaurantID(id: $id) {
      message
      success
      list {
        id
        nota
        isvalored
        aceptaTerminos
        usuario {
          _id
          name
          lastName
          email
          avatar
          city
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          OnesignalID
        }
        restaurant
        restaurants {
          _id
          title
          image
          description
          type
          rating
          address {
            calle
            ciudad
            codigoPostal
            numero
          }
          coordetate
          anadidoFavorito
          coordenateone
          categoryName
          categoryID
          minime
          city
          phone
          email
          logo
          password
          apertura
          cierre
          diaslaborales
          open
          OnesignalID
        }
        time
        cantidad
        userID
        propina
        platos {
          userId
          platoID
          restaurant
          complementos
          plato {
            _id
            title
            ingredientes
            price
            imagen
            restaurant
            menu
            oferta
            popular
            anadidoCart
            cant
          }
        }
        cubiertos
        estado
        status
        progreso
        created_at
        total
      }
    }
  }
`;

const GET_MENU = gql`
  query getMenu($id: ID) {
    getMenu(id: $id) {
      message
      success
      list {
        _id
        title
        subtitle
        restaurant
        platos {
          _id
          title
          ingredientes
          price
          imagen
          restaurant
          menu
          oferta
          popular
          anadidoCart
          news
          opiniones {
            id
            plato
            comment
            rating
            created_at
            user
          }
        }
      }
    }
  }
`;

const GET_ACOMPANANTE = gql`
  query getAcompanante($id: ID) {
    getAcompanante(id: $id) {
      message
      success
      list {
        id
        name
        plato
        restaurant
        children {
          id
          name
          price
          cant
        }
      }
    }
  }
`;

const GET_VALORACION = gql`
  query getValoraciones($restaurant: ID) {
    getValoraciones(restaurant: $restaurant) {
      messages
      success
      data {
        id
        comment
        value
        user
        created_at
        Usuario {
          _id
          name
          lastName
          email
          city
          avatar
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          OnesignalID
        }
      }
    }
  }
`;

const GET_NOTIFICATION = gql`
  query getNotifications($Id: ID) {
    getNotifications(Id: $Id) {
      messages
      success
      notifications {
        _id
        user
        usuario
        read
        ordenId
        restaurant
        type
        created_at
        Restaurant {
          _id
          title
          image
          description
          type
          logo
        }
        Usuarios {
          _id
          name
          lastName
          avatar
        }
        Orden {
          id
        }
      }
    }
  }
`;

const GET_TRANSACTION = gql`
  query getTransaction($id: ID) {
    getTransaction(id: $id) {
      messages
      success
      list {
        id
        estado
        fecha
        restaurantID
        created_at
        total
      }
    }
  }
`;

const GET_PAGO = gql`
  query getPago($id: ID) {
    getPago(id: $id) {
      messages
      success
      data {
        _id
        iban
        nombre
        restaurantID
      }
    }
  }
`;

const GET_STADISTIC = gql`
  query getStatistics($id: ID) {
    getStatistics(id: $id) {
      message
      success
      data {
        name
        Ene
        Feb
        Mar
        Abr
        May
        Jun
        Jul
        Aug
        Sep
        Oct
        Nov
        Dic
      }
    }
  }
`;

export const query = {
  GET_ORDEN,
  GET_ORDEN_FINISH,
  GET_ORDEN_PROCCESS,
  GET_ORDEN_NEW,
  RESTAURANT,
  GET_ORDEN_ID,
  GET_MENU,
  GET_ACOMPANANTE,
  GET_VALORACION,
  GET_NOTIFICATION,
  GET_TRANSACTION,
  GET_PAGO,
  GET_STADISTIC,
};
