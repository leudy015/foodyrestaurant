import { query } from "./Query";
import { mutations } from "./Mutation";

export { query, mutations };
