import gql from "graphql-tag";

const AUTENTICAR_RESTAURANT = gql`
  mutation autenticarRestaurant($email: String, $password: String) {
    autenticarRestaurant(email: $email, password: $password) {
      message
      success
      data {
        id
        token
      }
    }
  }
`;

const ORDEN_PROCEED = gql`
  mutation OrdenProceed(
    $orden: ID
    $estado: String
    $progreso: String
    $status: String
  ) {
    OrdenProceed(
      orden: $orden
      estado: $estado
      progreso: $progreso
      status: $status
    ) {
      messages
      success
    }
  }
`;

const CREATE_NOTIFICATION = gql`
  mutation createNotification($input: NotificationInput) {
    createNotification(input: $input) {
      messages
      success
    }
  }
`;

const READ_NOTIFICATION = gql`
  mutation readNotification($notificationId: ID) {
    readNotification(notificationId: $notificationId) {
      messages
      success
    }
  }
`;

const ACTUALIZAR_RESTAURANT = gql`
  mutation actualizarRestaurant($input: ActualizarRestaurantInput) {
    actualizarRestaurant(input: $input) {
      messages
      success
    }
  }
`;

export const UPLOAD_FILE = gql`
  mutation singleUpload($imgBlob: Upload) {
    singleUpload(file: $imgBlob) {
      filename
    }
  }
`;

const CREAR_PLATO = gql`
  mutation createPlatos($input: CreatePlatoInput) {
    createPlatos(input: $input) {
      messages
      success
    }
  }
`;

const CREAR_MENU = gql`
  mutation createMenu($input: CreateMenuInput) {
    createMenu(input: $input) {
      messages
      success
    }
  }
`;

const CREAR_ACOMPANANTE = gql`
  mutation createAcompanante($input: CreateAcompananteInput) {
    createAcompanante(input: $input) {
      messages
      success
    }
  }
`;

const ELIMINAR_MENU = gql`
  mutation eliminarMenu($id: ID) {
    eliminarMenu(id: $id) {
      messages
      success
    }
  }
`;

const ELIMINAR_PLATO = gql`
  mutation eliminarPlato($id: ID) {
    eliminarPlato(id: $id) {
      messages
      success
    }
  }
`;

const ELIMINAR_COMPLEMENTO = gql`
  mutation eliminarComplemento($id: ID) {
    eliminarComplemento(id: $id) {
      messages
      success
    }
  }
`;

const CREAR_PAGO = gql`
  mutation crearPago($input: PagoInput) {
    crearPago(input: $input) {
      messages
      success
    }
  }
`;

const ELIMINAR_PAGO = gql`
  mutation eliminarPago($id: ID) {
    eliminarPago(id: $id) {
      messages
      success
    }
  }
`;

export const mutations = {
  AUTENTICAR_RESTAURANT,
  ORDEN_PROCEED,
  CREATE_NOTIFICATION,
  READ_NOTIFICATION,
  ACTUALIZAR_RESTAURANT,
  UPLOAD_FILE,
  CREAR_PLATO,
  CREAR_MENU,
  CREAR_ACOMPANANTE,
  ELIMINAR_MENU,
  ELIMINAR_PLATO,
  ELIMINAR_COMPLEMENTO,
  CREAR_PAGO,
  ELIMINAR_PAGO,
};
