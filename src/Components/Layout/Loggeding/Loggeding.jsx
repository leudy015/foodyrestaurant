import React from "react";
import { Grid } from "semantic-ui-react";
import { BrowserRouter as Routers } from "react-router-dom";
import Router from "../../../Router/router";
import MenuContent from "../../MenuLeft";
import TopBar from "../../TopBar";

import "./Logueding.scss";

export default function Loggeding(props) {
  const { data } = props;
  return (
    <Routers>
      <Grid className="logged-layout">
        <Grid.Row>
          <Grid.Column width={3}>
            <MenuContent data={data} />
          </Grid.Column>
          <Grid.Column width={13}>
            <TopBar res={data} />
            <div className="content">
              <Router data={data} />
            </div>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Routers>
  );
}
