import React, { useState, useEffect } from "react";
import { Menu, Icon, Button } from "semantic-ui-react";
import { Link, withRouter } from "react-router-dom";
import BasicModal from "../../Components/Modal/BasicModal";

import "./menu.scss";

function Menus(props) {
  const { location, history } = props;
  const [activeMenu, setactiveMenu] = useState(location.pathname);
  const [showModal, setshowModal] = useState(false);
  const [title, settitle] = useState("");
  const [contentModal, setcontentModal] = useState(null);
  const [size, setsize] = useState("tiny");

  useEffect(() => {
    setactiveMenu(location.pathname);
  }, [location]);

  const handleMenu = (menu) => {
    setactiveMenu(menu.to);
  };

  function Modalcontent() {
    return (
      <div className="content-modal">
        <h3>Cerrar sesión</h3>
        <p>¿Seguro que quieres cerra sesión en Foody?</p>
        <Button className="accept" onClick={() => cerra()}>
          Cerrar sesión
        </Button>
        <Button className="cancel" onClick={() => setshowModal(false)}>
          Cancelar
        </Button>
      </div>
    );
  }

  function ModalcontentContact() {
    return (
      <div className="content-modal">
        <h3>¿Tienes alguna duda?</h3>
        <p>Puede llamarnos estamos disponible 24/7 para ayudarte</p>

        <p>Teléfono: +34 689 351 592</p>
        <p>Email: restaurant@foodyapp.es</p>

        <Button className="cancel" onClick={() => setshowModal(false)}>
          Ok
        </Button>
      </div>
    );
  }

  const cerra = () => {
    setshowModal(false);
    localStorage.removeItem("token");
    localStorage.removeItem("id");
    history.goBack();
  };

  const handleModal = (type) => {
    switch (type) {
      case "cerrar sesion":
        settitle("Cerrar sesión");
        setcontentModal(Modalcontent);
        setshowModal(true);
        setsize("tiny");
        break;

      default:
        settitle("Contacta con nosotros");
        setcontentModal(ModalcontentContact);
        setshowModal(true);
        setsize("tiny");
        break;
    }
  };

  return (
    <>
      <Menu className="nemu-left" vertical>
        <div className="top">
          <Menu.Item
            name="pedidos"
            as={Link}
            to="/"
            active={activeMenu === "/"}
            onClick={handleMenu}
          >
            <Icon name="file alternate outline" /> Pedidos
          </Menu.Item>
          <Menu.Item
            name="restaurant"
            as={Link}
            to="/restaurant"
            active={activeMenu === "/restaurant"}
            onClick={handleMenu}
          >
            <Icon name="home" /> Restaurante
          </Menu.Item>

          <Menu.Item
            name="menu"
            as={Link}
            to="/menu"
            active={activeMenu === "/menu"}
            onClick={handleMenu}
          >
            <Icon name="list layout" /> Menú
          </Menu.Item>

          <Menu.Item
            name="pago"
            as={Link}
            to="/pago"
            active={activeMenu === "/pago"}
            onClick={handleMenu}
          >
            <Icon name="payment" /> Datos de cobro
          </Menu.Item>

          <Menu.Item
            name="opiniones"
            as={Link}
            to="/opiniones"
            active={activeMenu === "/opiniones"}
            onClick={handleMenu}
          >
            <Icon name="star" /> Opiniones
          </Menu.Item>

          <Menu.Item
            name="transacciones"
            as={Link}
            to="/transacciones"
            active={activeMenu === "/transacciones"}
            onClick={handleMenu}
          >
            <Icon name="arrows alternate horizontal" /> Transacciones
          </Menu.Item>

          <Menu.Item
            name="estadisticas"
            as={Link}
            to="/estadisticas"
            active={activeMenu === "/estadisticas"}
            onClick={handleMenu}
          >
            <Icon name="chart bar" /> Estadísticas
          </Menu.Item>
        </div>
        <div className="footer">
          <Menu.Item onClick={() => handleModal("cerrar sesion")}>
            <Icon name="log out" /> Cerrar sesión
          </Menu.Item>
          <Menu.Item onClick={() => handleModal()}>
            <Icon name="help circle" /> Ayuda y contacto
          </Menu.Item>
        </div>
      </Menu>
      <BasicModal
        show={showModal}
        setshow={setshowModal}
        title={title}
        size={size}
      >
        {contentModal}
      </BasicModal>
    </>
  );
}

export default withRouter(Menus);
