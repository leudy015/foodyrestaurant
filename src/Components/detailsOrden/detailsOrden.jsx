import React, { useState } from "react";
import { Grid, Button } from "semantic-ui-react";
import { query, mutations } from "../../GraphQL";
import { Query, useMutation } from "react-apollo";
import { withRouter } from "react-router-dom";
import { IMAGES_PATH } from "../../config";
import moment from "moment";
import "moment/locale/es";
import { toast } from "react-toastify";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import { LOCAL_API_URL } from "../../config";

import "./detailsOrden.scss";

function DetailsOrden(props) {
  const [Loading, setLoading] = useState(false);
  const [OrdenProceed] = useMutation(mutations.ORDEN_PROCEED);
  const [createNotification] = useMutation(mutations.CREATE_NOTIFICATION);
  const id = props.match.params.id;
  return (
    <Query query={query.GET_ORDEN_ID} variables={{ id: id }}>
      {(response) => {
        if (response.loading) {
          return (
            <div className="loading">
              <SkeletonTheme color="#202020" highlightColor="#444">
                <div>
                  <Skeleton height={250} width="100%" />
                </div>
              </SkeletonTheme>
              <SkeletonTheme color="#202020" highlightColor="#444">
                <div>
                  <Skeleton height={250} width="100%" />
                </div>
              </SkeletonTheme>
              <SkeletonTheme color="#202020" highlightColor="#444">
                <div>
                  <Skeleton height={250} width="100%" />
                </div>
              </SkeletonTheme>
              <SkeletonTheme color="#202020" highlightColor="#444">
                <div>
                  <Skeleton height={250} width="100%" />
                </div>
              </SkeletonTheme>
              <SkeletonTheme color="#202020" highlightColor="#444">
                <div>
                  <Skeleton height={250} width="100%" />
                </div>
              </SkeletonTheme>
              <SkeletonTheme color="#202020" highlightColor="#444">
                <div>
                  <Skeleton height={250} width="100%" />
                </div>
              </SkeletonTheme>
              <SkeletonTheme color="#202020" highlightColor="#444">
                <div>
                  <Skeleton height={250} width="100%" />
                </div>
              </SkeletonTheme>
            </div>
          );
        }
        if (response) {
          const respuesta =
            response && response.data && response.data.getOrderByRestaurantID
              ? response.data.getOrderByRestaurantID.list
              : {};

          response.refetch();
          let donde = "";
          let asetijo = "";
          switch (respuesta.time) {
            case "Comer aquí":
              donde = "Comer aquí";
              asetijo = "Para";
              break;
            case "Recoger en 25 min":
              donde = "Recoger en 25 min";
              asetijo = "Para";
              break;
            default:
              donde = moment(respuesta.time).format("lll");
              asetijo = "Para recoger";
              break;
          }

          const SendPushNotificationNeworden = (
            OnesignalID,
            messageTexteNeworden
          ) => {
            fetch(
              `${LOCAL_API_URL}/send-push-notification?IdOnesignal=${OnesignalID}&textmessage=${messageTexteNeworden}`
            ).catch((err) => err);
          };

          const createNotifications = (
            user,
            restaurant,
            usuario,
            ordenId,
            type
          ) => {
            const NotificationInput = {
              user,
              restaurant,
              ordenId,
              usuario,
              type,
            };

            createNotification({
              variables: { input: NotificationInput },
            })
              .then(async (results) => {})
              .catch((err) => {});
          };

          const proccessOrder = (
            order,
            estado,
            progreso,
            status,
            usuario,
            restaurant,
            type,
            messageTexteNeworden,
            OnesignalID
          ) => {
            setLoading(true);
            OrdenProceed({
              variables: {
                orden: order,
                estado: estado,
                progreso: progreso,
                status: status,
              },
            })
              .then((res) => {
                if (res.data.OrdenProceed.success) {
                  SendPushNotificationNeworden(
                    OnesignalID,
                    messageTexteNeworden
                  );
                  createNotifications(
                    usuario,
                    restaurant,
                    usuario,
                    order,
                    type
                  );
                  response.refetch();
                  toast.success(res.data.OrdenProceed.messages, {
                    position: "top-center",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                  });
                } else {
                  response.refetch();
                  toast.error(res.data.OrdenProceed.messages, {
                    position: "top-center",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                  });
                }
              })
              .then((err) => err)
              .finally(() => {
                setLoading(false);
                response.refetch();
              });
          };

          return (
            <>
              <div
                className="barner"
                style={{
                  backgroundImage: `linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(${respuesta.platos[0].plato.imagen})`,
                }}
              >
                <div className="barner__logo">
                  <img src={respuesta.restaurants.logo} alt="" />
                  <h2>{respuesta.restaurants.title}</h2>
                </div>
              </div>

              <div className="restaurant">
                <Grid>
                  <Grid.Row>
                    <Grid.Column width={8}>
                      <div className="restaurant__container-details">
                        <h4>{respuesta.estado}</h4>
                        <p>{moment(respuesta.created_at).format("lll")}</p>
                        <p>id de foody {respuesta.id.slice(0, 8)}</p>
                      </div>
                      <div className="restaurant__container-details2">
                        <h4>{asetijo}</h4>
                        <p>{donde}</p>
                      </div>
                      <div className="restaurant__container-details2">
                        <h4>Nota del cliente</h4>
                        <p style={{ width: 400 }}>{respuesta.nota}</p>
                      </div>

                      <div className="restaurant__container-details2">
                        <h4>¿Incluir cubiertos?</h4>
                        <p>{respuesta.cubiertos ? "Si" : "No"}</p>
                      </div>

                      {respuesta.propina ? (
                        <div className="restaurant__container-details2">
                          <h4>
                            {respuesta.usuario.name}{" "}
                            {respuesta.usuario.lastName}
                          </h4>
                          <p>Te ha dejado propina</p>
                        </div>
                      ) : null}

                      <div className="restaurant__container-details2">
                        <h4>Cliente</h4>
                        <div className="cont">
                          <img
                            src={IMAGES_PATH + respuesta.usuario.avatar}
                            alt={respuesta.usuario.name}
                          />
                          <div>
                            <h3>
                              {respuesta.usuario.name}{" "}
                              {respuesta.usuario.lastName}
                            </h3>
                            <p>{respuesta.usuario.city}</p>
                            <p>+ {respuesta.usuario.telefono}</p>
                          </div>
                        </div>
                      </div>
                    </Grid.Column>
                    <Grid.Column width={8}>
                      <div className="container-prod">
                        <h2>Información de productos</h2>

                        {respuesta.platos.map((c, i) => {
                          return (
                            <div className="mini-card" key={i}>
                              <img src={c.plato.imagen} alt={c.plato.title} />
                              <div>
                                <h3>
                                  {" "}
                                  <span>{c.plato.cant} × </span>
                                  {c.plato.title}
                                </h3>
                                {c &&
                                  c.complementos.map((e, i) => (
                                    <p key={i}>{e}</p>
                                  ))}

                                <p>{c.plato.ingredientes}</p>
                              </div>
                            </div>
                          );
                        })}
                        <div className="btn-proccess">
                          {respuesta.estado === "Pagada" ? (
                            <>
                              <Button
                                loading={Loading}
                                onClick={() =>
                                  proccessOrder(
                                    respuesta.id,
                                    "Confirmada",
                                    "50",
                                    "active",
                                    respuesta.userID,
                                    respuesta.restaurant,
                                    "accept_order",
                                    `Genial ${respuesta.restaurants.title} ha confirmado tu pedido.`,
                                    respuesta.usuario.OnesignalID
                                  )
                                }
                              >
                                Confirmar
                              </Button>
                              <Button
                                loading={Loading}
                                className="cancel"
                                onClick={() =>
                                  proccessOrder(
                                    respuesta.id,
                                    "Rechazada",
                                    "0",
                                    "exception",
                                    respuesta.userID,
                                    respuesta.restaurant,
                                    "reject_order",
                                    `Upps ${respuesta.restaurants.title} no ha podido realizar tu pedido lo sentimos.`,
                                    respuesta.usuario.OnesignalID
                                  )
                                }
                              >
                                Cacelar
                              </Button>
                            </>
                          ) : null}

                          {respuesta.estado === "Confirmada" ? (
                            <Button
                              loading={Loading}
                              onClick={() =>
                                proccessOrder(
                                  respuesta.id,
                                  "Preparando",
                                  "75",
                                  "active",
                                  respuesta.userID,
                                  respuesta.restaurant,
                                  "order_process",
                                  `${respuesta.restaurants.title} está preparando tu pedido en la cocina.`,
                                  respuesta.usuario.OnesignalID
                                )
                              }
                            >
                              Preparando
                            </Button>
                          ) : null}

                          {respuesta.estado === "Preparando" ? (
                            <Button
                              loading={Loading}
                              onClick={() =>
                                proccessOrder(
                                  respuesta.id,
                                  "Lista para recojer",
                                  "100",
                                  "active",
                                  respuesta.userID,
                                  respuesta.restaurant,
                                  "finish_order",
                                  `Tu pedido está listo para recojer en ${respuesta.restaurants.title}`,
                                  respuesta.usuario.OnesignalID
                                )
                              }
                            >
                              Listo para recojer
                            </Button>
                          ) : null}

                          {respuesta.estado === "Lista para recojer" ? (
                            <Button
                              loading={Loading}
                              onClick={() =>
                                proccessOrder(
                                  respuesta.id,
                                  "Recogido",
                                  "100",
                                  "success",
                                  respuesta.userID,
                                  respuesta.restaurant,
                                  "valored_order",
                                  `Valora tu experiencia con ${respuesta.restaurants.title} y tambien opina sobre los platos que compraste para ayudar a otros clientes a decidir mejor`,
                                  respuesta.usuario.OnesignalID
                                )
                              }
                            >
                              Entregado
                            </Button>
                          ) : null}
                        </div>
                      </div>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </div>
            </>
          );
        }
      }}
    </Query>
  );
}

export default withRouter(DetailsOrden);
