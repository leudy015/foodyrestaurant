import React from "react";
import { Modal } from "semantic-ui-react";

import "./AcompananteModal.scss";

export default function BasicModal(props) {
  const { show, setshow, title, children, size, imagen } = props;

  const Onclose = () => {
    setshow(false);
  };
  return (
    <Modal open={show} onClose={Onclose} className="basic-modal" size={size}>
      <Modal.Header>
        <img src={imagen} alt={title} />
      </Modal.Header>
      <Modal.Content>{children}</Modal.Content>
    </Modal>
  );
}
