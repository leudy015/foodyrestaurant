import React from "react";
import { Modal, Icon } from "semantic-ui-react";

import "./BasicModal.scss";

export default function BasicModal(props) {
  const { show, setshow, children, size } = props;

  const Onclose = () => {
    setshow(false);
  };
  return (
    <Modal open={show} onClose={Onclose} className="basic-modal" size={size}>
      <Modal.Header>
        <Icon name="close" onClick={Onclose} />
      </Modal.Header>
      <Modal.Content>{children}</Modal.Content>
    </Modal>
  );
}
