import React, { useState } from "react";
import { withRouter } from "react-router-dom";
import { Input, Button, Icon, Checkbox } from "semantic-ui-react";
import { mutations } from "../../GraphQL";
import { useMutation } from "react-apollo";
import { toast } from "react-toastify";

import "./add.scss";

function Add(props) {
  const { res, history } = props;
  const platoID = props.match.params.id;
  const [children, setChildren] = useState(setChildre());
  const [hijos, setHijos] = useState([]);
  const [loadinAcomp, setloadinAcomp] = useState(false);
  const [acomp, setAcomp] = useState("");
  const [required, setRequired] = useState(false);
  const [createAcompanante] = useMutation(mutations.CREAR_ACOMPANANTE);

  const handleChange = (e, { checked }) => setRequired(checked);

  function removeItemFromArr(arr, item) {
    var i = arr.indexOf(item);
    if (i !== -1) {
      arr.splice(i, 1);
      setHijos(hijos.concat());
    }
  }

  const onChageFormDataAcomp = (e) => {
    setChildren({
      ...children,
      [e.target.name]: e.target.value,
    });
  };

  const addChildren = () => {
    setChildren({
      id: Math.floor(Math.random(5, 10) * (10000000 + 1)),
      name: children.name,
      price: children.price ? `${children.price}` : null,
      cant: children.cant,
    });

    if (children.name === "") {
      alert("Debes añadir una opción");
    } else {
      setHijos(hijos.concat(children));
    }
  };

  const anadirAcompanante = (restaurant, platosAcopm) => {
    setloadinAcomp(true);
    const input = {
      name: acomp,
      children: hijos,
      restaurant: restaurant,
      plato: platosAcopm,
      required: required,
    };

    console.log(input);

    if (acomp === "") {
      alert("Debes añadir un titulo para continuar");
    } else {
      if (children.name === "") {
        alert("Debes añadir opciones para continuar");
      } else {
        createAcompanante({ variables: { input: input } })
          .then((res) => {
            if (res.data.createAcompanante.success) {
              toast.success("Opción añadida con éxito", {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
              });
            } else {
              toast.error(
                "Hubo un error con su solicitud vulve a intentarlo por favor",
                {
                  position: "top-center",
                  autoClose: 5000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
                }
              );
            }
          })
          .catch((error) => {
            toast.error(
              "Hubo un error con su solicitud vulve a intentarlo por favor",
              {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
              }
            );
          })
          .finally(() => setloadinAcomp(false));
        history.goBack();
      }
    }
  };

  const onTagsChanged = (ite) => {
    removeItemFromArr(hijos, ite);
  };

  return (
    <div className="anadir-opciones">
      <h3>Añadir opciones</h3>
      <Input
        placeholder="Añade un nombre  (Ejm Selecciona tu masa favorita)"
        onChange={(e) => setAcomp(e.target.value)}
        style={{ width: "93%" }}
      />
      <Checkbox
        name="required"
        label="Marcar como requerido"
        onChange={handleChange}
      />
      {hijos.map((j, i) => {
        return (
          <div key={i}>
            <div style={{ display: "flex" }}>
              <p>
                {j.name} {j.price ? `(+ ${j.price} €)` : null}
              </p>
              <Icon
                name="trash"
                link
                onClick={() => onTagsChanged(j)}
                style={{
                  marginLeft: "auto",
                  marginRight: 20,
                  paddingTop: 20,
                  color: "#f5365c",
                }}
              />
            </div>

            <div className="separator" />
          </div>
        );
      })}

      <div style={{ margin: 20 }}>
        <Input
          placeholder="Añade un nombre  (Ejm Masa fina, masa normal, etc.)"
          name="name"
          style={{ marginTop: 20, width: "93%" }}
          onChange={onChageFormDataAcomp}
        />
        <div>
          <Input
            placeholder="Precio Ejemplo (0.50) Opcional"
            name="price"
            style={{ width: "93%" }}
            onChange={onChageFormDataAcomp}
          />
        </div>
        <Button
          onClick={() => addChildren()}
          loading={loadinAcomp}
          style={{ marginLeft: 15, backgroundColor: "#3c3c3c" }}
        >
          Incluir
        </Button>
      </div>

      <Button
        onClick={() => anadirAcompanante(res._id, platoID)}
        loading={loadinAcomp}
      >
        Añadir opción
      </Button>
    </div>
  );
}

function setChildre() {
  return {
    id: Math.floor(Math.random(5, 10) * (10000000 + 1)),
    name: "",
    price: "",
    cant: 1,
  };
}

export default withRouter(Add);
