import React from "react";
import { withRouter } from "react-router-dom";
import { query, mutations } from "../../GraphQL";
import { useQuery, useMutation } from "react-apollo";
import { IMAGES_PATH } from "../../config";
import moment from "moment";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import "./notification.scss";

function Valoraciones(props) {
  const { res } = props;
  const [readNotification] = useMutation(mutations.READ_NOTIFICATION);

  const { data, loading, refetch } = useQuery(query.GET_NOTIFICATION, {
    variables: { Id: res._id },
  });

  const readNotifications = (id) => {
    readNotification({ variables: { notificationId: id } })
      .then(() => {})
      .catch((err) => err);
  };

  if (loading) {
    return (
      <div className="valoraciones">
        <SkeletonTheme color="#202020" highlightColor="#444">
          <div style={{ marginTop: 20 }}>
            <Skeleton height={120} width="100%" />
          </div>
        </SkeletonTheme>
        <SkeletonTheme color="#202020" highlightColor="#444">
          <div style={{ marginTop: 20 }}>
            <Skeleton height={120} width="100%" />
          </div>
        </SkeletonTheme>
        <SkeletonTheme color="#202020" highlightColor="#444">
          <div style={{ marginTop: 20 }}>
            <Skeleton height={120} width="100%" />
          </div>
        </SkeletonTheme>
        <SkeletonTheme color="#202020" highlightColor="#444">
          <div style={{ marginTop: 20 }}>
            <Skeleton height={120} width="100%" />
          </div>
        </SkeletonTheme>
        <SkeletonTheme color="#202020" highlightColor="#444">
          <div style={{ marginTop: 20 }}>
            <Skeleton height={120} width="100%" />
          </div>
        </SkeletonTheme>
        <SkeletonTheme color="#202020" highlightColor="#444">
          <div style={{ marginTop: 20 }}>
            <Skeleton height={120} width="100%" />
          </div>
        </SkeletonTheme>
      </div>
    );
  }

  const datos = data.getNotifications.notifications;

  refetch();
  return (
    <div className="valoraciones">
      <h3 className="titles" style={{ paddingTop: 30 }}>
        Notificaciones
      </h3>
      {datos.length > 0 ? (
        <>
          {datos.map((d, i) => {
            let title = "";
            let description = "";
            let avatar = "";
            // eslint-disable-next-line
            switch (d.type) {
              case "new_order":
                title = "Nuevo pedido";
                description = `Has recibido un nuevo pedido de ${d.Usuarios.name} ${d.Usuarios.lastName}.`;
                avatar = d.Usuarios.avatar;
                break;
            }

            return (
              <div
                className="card__container"
                key={i}
                onClick={() => readNotifications(d._id)}
              >
                <div className="card__header" style={{ display: "flex" }}>
                  <img src={IMAGES_PATH + avatar} alt="" className="avatar" />
                  <div style={{ marginLeft: 15 }}>
                    <h4 className="name">{title}</h4>
                    <p className="date">
                      {moment(Number(d.created_at)).fromNow()}
                    </p>
                  </div>
                </div>
                <p className="comments">{description}</p>
              </div>
            );
          })}
        </>
      ) : (
        <div style={{ textAlign: "center", paddingBottom: 30 }}>
          <img
            src="https://api.foodyapp.es/assets/images/notification.png"
            alt="crear menu"
            style={{ width: 250 }}
          />
          <h3 style={{ color: "white" }}>
            Estás al día no tienes notificaciones.
          </h3>
        </div>
      )}
    </div>
  );
}

export default withRouter(Valoraciones);
