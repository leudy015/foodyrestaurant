import React, { useState } from "react";
import { Grid, Input, Button, TextArea, Icon } from "semantic-ui-react";
import { TimePicker } from "antd";
import { mutations, query } from "../../GraphQL";
import { useMutation, Query } from "react-apollo";
import { toast } from "react-toastify";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import "./restaurant.scss";

export default function Restaurant(props) {
  const { data } = props;
  const [apertura, setApertura] = useState();
  const [cierre, setCierre] = useState();
  const [tags, setTags] = useState(data.diaslaborales);
  const [dia, setDias] = useState(SetElements());
  const [formData, setFormData] = useState(SetElementos());
  const [direccion, setDireccion] = useState(Setdirection());
  const [loading, setLoading] = useState(false);
  const [actualizarRestaurant] = useMutation(mutations.ACTUALIZAR_RESTAURANT);

  function removeItemFromArr(arr, item) {
    var i = arr.indexOf(item);
    if (i !== -1) {
      arr.splice(i, 1);
      setTags(tags.concat());
    }
  }

  const onTagsChanged = (tag) => {
    removeItemFromArr(tags, tag);
  };

  const onChageDia = (e) => {
    setDias({
      ...dia,
      [e.target.name]: e.target.value,
    });
    e.preventDefault();
  };

  const onChageFormData = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };

  const onChagedirection = (e) => {
    setDireccion({
      ...direccion,
      [e.target.name]: e.target.value,
    });
  };

  const anadirdia = () => {
    if (dia.dia === "") {
      alert("Debes añadir un día de la semana");
    } else {
      if (tags.length > 6) {
        alert("Has añadido el número máximo de día");
      } else {
        setTags(tags.concat(dia.dia));
      }
    }
  };

  const saveInfo = (id) => {
    setLoading(true);
    const input = {
      _id: id ? id : data._id,
      title: formData.title ? formData.title : data.title,
      description: formData.description
        ? formData.description
        : data.description,
      address: {
        calle: direccion.calle ? direccion.calle : data.address.calle,
        numero: direccion.numero ? direccion.numero : data.address.numero,
        codigoPostal: direccion.codigoPostal
          ? direccion.codigoPostal
          : data.address.codigoPostal,
        ciudad: direccion.ciudad ? direccion.ciudad : data.address.ciudad,
      },
      phone: formData.phone ? formData.phone : data.phone,
      apertura: apertura ? apertura : data.apertura,
      cierre: cierre ? cierre : data.cierre,
      diaslaborales: tags,
    };
    actualizarRestaurant({ variables: { input: input } })
      .then((res) => {
        if (res.data.actualizarRestaurant.success) {
          toast.success(res.data.actualizarRestaurant.messages, {
            position: "top-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        } else {
          toast.error(res.data.actualizarRestaurant.messages, {
            position: "top-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
      })
      .then((err) => err)
      .finally(() => setLoading(false));
  };

  function onChangeTimeabre(time, timeString) {
    setApertura(timeString);
  }

  function onChangeTimecierra(time, timeString) {
    setCierre(timeString);
  }

  return (
    <>
      <div
        className="barner"
        style={{
          backgroundImage: `linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url("${data.image}")`,
        }}
      >
        <div className="barner__logo">
          <img src={data.logo} alt={data.title} />
          <h2>{data.title}</h2>
        </div>
      </div>

      <div className="restaurant">
        <Grid className="lrestaurant-layout">
          <Grid.Row>
            <Grid.Column width={8}>
              <h3>Información basica</h3>
              <Input
                placeholder={data.title}
                name="title"
                onChange={onChageFormData}
              />
              <TextArea
                name="description"
                placeholder={data.description}
                onChange={onChageFormData}
              />

              <Input
                placeholder={data.phone}
                name="phone"
                onChange={onChageFormData}
              />

              <TimePicker
                onChange={onChangeTimeabre}
                style={{ width: 170 }}
                placeholder="Hora de apertura"
              />
              <TimePicker
                onChange={onChangeTimecierra}
                style={{ marginLeft: 20, width: 170 }}
                placeholder="Hora de cierre"
              />

              <h3>Dirección</h3>

              <Input
                placeholder={data.address.calle}
                name="calle"
                onChange={onChagedirection}
              />
              <Input
                placeholder={data.address.numero}
                name="numero"
                onChange={onChagedirection}
              />
              <Input
                placeholder={data.address.codigoPostal}
                name="codigoPostal"
                onChange={onChagedirection}
              />
              <Input
                placeholder={data.address.ciudad}
                name="ciudad"
                onChange={onChagedirection}
              />

              <h3>Días laborables</h3>

              <div className="dias">
                {tags.map((o, i) => (
                  <div>
                    <Icon
                      name="close"
                      link
                      onClick={() => onTagsChanged(o)}
                      style={{ color: "#f5365c" }}
                    />
                    <p key={i}>{o}</p>
                  </div>
                ))}
              </div>
              <Input
                name="dia"
                placeholder="Añadir día"
                onChange={onChageDia}
                style={{ marginTop: 20 }}
              />
              <Button
                onClick={() => anadirdia()}
                style={{ backgroundColor: "rgb(153, 153, 153)" }}
              >
                Añadir día
              </Button>

              <Button onClick={() => saveInfo(data._id)} loading={loading}>
                Guardar Cambios
              </Button>
            </Grid.Column>
            <Grid.Column width={8}>
              <Query query={query.GET_MENU} variables={{ id: data._id }}>
                {(response) => {
                  if (response.loading) {
                    return (
                      <>
                        <SkeletonTheme color="#202020" highlightColor="#444">
                          <div style={{ marginTop: 35 }}>
                            <Skeleton height={170} width="100%" />
                          </div>
                        </SkeletonTheme>
                        <SkeletonTheme color="#202020" highlightColor="#444">
                          <div style={{ marginTop: 35 }}>
                            <Skeleton height={170} width="100%" />
                          </div>
                        </SkeletonTheme>
                        <SkeletonTheme color="#202020" highlightColor="#444">
                          <div style={{ marginTop: 35 }}>
                            <Skeleton height={170} width="100%" />
                          </div>
                        </SkeletonTheme>
                        <SkeletonTheme color="#202020" highlightColor="#444">
                          <div style={{ marginTop: 35 }}>
                            <Skeleton height={170} width="100%" />
                          </div>
                        </SkeletonTheme>
                        <SkeletonTheme color="#202020" highlightColor="#444">
                          <div style={{ marginTop: 35 }}>
                            <Skeleton height={170} width="100%" />
                          </div>
                        </SkeletonTheme>
                        <SkeletonTheme color="#202020" highlightColor="#444">
                          <div style={{ marginTop: 35 }}>
                            <Skeleton height={170} width="100%" />
                          </div>
                        </SkeletonTheme>
                      </>
                    );
                  }
                  if (response) {
                    const respuesta =
                      response && response.data && response.data.getMenu
                        ? response.data.getMenu.list
                        : [];

                    response.refetch();
                    return (
                      <div>
                        <h3>Menú</h3>

                        {respuesta.length > 0 ? (
                          <>
                            {respuesta.map((da, i) => {
                              return (
                                <div key={i}>
                                  <h4>{da.title}</h4>
                                  <p>{da.subtitle}</p>

                                  {da.platos.map((c, i) => {
                                    return (
                                      <div className="mini-card" key={i}>
                                        <img src={c.imagen} alt={c.title} />
                                        <div>
                                          <h3>{c.title}</h3>
                                          <p>{c.ingredientes}</p>
                                          <div
                                            style={{
                                              display: "flex",
                                            }}
                                          >
                                            <p>{c.price}€</p>

                                            {c.popular ? (
                                              <p
                                                style={{
                                                  color: "#ffa500",
                                                  marginLeft: 15,
                                                }}
                                              >
                                                Popular
                                              </p>
                                            ) : null}

                                            {c.oferta ? (
                                              <p
                                                style={{
                                                  color: "#95ca3e",
                                                  marginLeft: 15,
                                                }}
                                              >
                                                Oferta
                                              </p>
                                            ) : null}
                                          </div>
                                        </div>
                                      </div>
                                    );
                                  })}
                                </div>
                              );
                            })}
                          </>
                        ) : (
                          <div style={{ textAlign: "center" }}>
                            <img
                              src="https://api.foodyapp.es/assets/images/nodata.png"
                              alt="crear menu"
                              className="nocontenimage"
                              style={{ width: 200 }}
                            />
                            <h3>
                              Aún no has creado un menú para tu restaurante.
                            </h3>
                          </div>
                        )}
                      </div>
                    );
                  }
                }}
              </Query>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    </>
  );
}

function SetElements() {
  return { dia: "" };
}

function SetElementos() {
  return {
    title: "",
    description: "",
    phone: "",
  };
}

function Setdirection() {
  return {
    calle: "",
    numero: "",
    codigoPostal: "",
    ciudad: "",
  };
}
