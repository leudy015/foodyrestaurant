import React from "react";
import { withRouter } from "react-router-dom";

import "./search.scss";

function Search() {
  return (
    <div className="search">
      <div style={{ textAlign: "center", paddingBottom: 50 }}>
        <img
          src="https://api.foodyapp.es/assets/images/noresult.png"
          alt="crear menu"
          style={{ width: 250 }}
        />
        <h3 style={{ color: "white", marginTop: -30 }}>
          Aún no hay resultado para está busqueda.
        </h3>
      </div>
    </div>
  );
}

export default withRouter(Search);
