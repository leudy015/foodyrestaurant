import React from "react";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import { Button } from "semantic-ui-react";
import { Line } from "rc-progress";
import { withRouter } from "react-router-dom";
import moment from "moment";
import "moment/locale/es";

import "./card.scss";

function Card(props) {
  const { data, loading, refetch, history } = props;
  refetch();
  return (
    <>
      {data.map((da, i) => {
        const plato = da.platos[0].plato;
        const platos = da.platos[0];
        let donde = "";
        let asetijo = "";
        switch (da.time) {
          case "Comer aquí":
            donde = "Comer aquí";
            asetijo = "Para";
            break;
          case "Recoger en 25 min":
            donde = "Recoger en 25 min";
            asetijo = "Para";
            break;
          default:
            donde = moment(da.time).format("lll");
            asetijo = "Para recoger";
            break;
        }

        let color = "#ffcb40";
        // eslint-disable-next-line
        switch (da.progreso) {
          case "25":
            color = "#ffcb40";
            break;

          case "50":
            color = "#ffcb40";
            break;

          case "75":
            color = "#ffcb40";
            break;

          case "100":
            color = "#95ca3e";
            break;
        }

        const navigation = (id) => {
          history.push(`/details-orden/${id}`);
        };

        return (
          <SkeletonTheme color="#202020" highlightColor="#444" key={i}>
            <div className="card">
              <div className="card__imgs">
                {loading ? (
                  <Skeleton height={100} width={100} />
                ) : (
                  <img src={plato.imagen} alt={plato.title} />
                )}
              </div>
              <div className="card__contaiter">
                {da.platos.map((e, i) => {
                  return (
                    <h4 key={i}>
                      {" "}
                      <span>{e.plato.cant} × </span>
                      {e.plato.title}
                    </h4>
                  );
                })}
                {platos.complementos.map((com, i) => {
                  return <p key={i}>{com}</p>;
                })}
              </div>
              <div className="card__contaiter1">
                <h4>Estado de la orden</h4>
                <h3
                  style={{
                    color: da.estado === "Rechazada" ? "#f5365c" : "#95ca3e",
                  }}
                >
                  {da.estado}
                </h3>

                <h4>{asetijo}</h4>
                <h3>{donde}</h3>
              </div>

              <div className="card__contaiter2">
                <h4>Progreso</h4>
                <Line
                  percent={da.progreso}
                  strokeWidth="4"
                  strokeColor={color}
                />

                <h4 style={{ marginTop: 20 }}>Total del pedido</h4>
                <h3>{da.total} €</h3>
              </div>

              <div className="card__contaiter3">
                {da.estado === "Pagada" ? <div>Nueva</div> : null} <br />
                <Button loading={loading} onClick={() => navigation(da.id)}>
                  Ver detalles
                </Button>
              </div>
            </div>
          </SkeletonTheme>
        );
      })}
    </>
  );
}

export default withRouter(Card);
