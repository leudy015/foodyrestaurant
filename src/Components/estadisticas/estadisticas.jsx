import { Chart, Tooltip, Axis, Legend, Bar } from "viser-react";
import * as React from "react";
import { withApollo, Query } from "react-apollo";
import { query } from "../../GraphQL";
import "./estadisticas.scss";
const DataSet = require("@antv/data-set");

class Estadisticas extends React.Component {
  render() {
    return (
      <div className="restaurant">
        <h3 style={{ marginBottom: 50 }}>Estadísticas</h3>
        <Query
          query={query.GET_STADISTIC}
          variables={{ id: localStorage.getItem("id") }}
        >
          {({ loading, error, data, refetch }) => {
            refetch();
            if (loading) return null;

            if (error) return null;
            const dv = new DataSet.View().source(data.getStatistics.data);
            dv.transform({
              type: "fold",
              fields: [
                "Ene",
                "Feb",
                "Mar",
                "Abr",
                "May",
                "Jun",
                "Jul",
                "Aug",
                "Sep",
                "Oct",
                "Nov",
                "Dic",
              ],
              key: "locatefit",
              value: "estadistica",
            });
            const sourceData = dv.rows;
            return (
              <Chart forceFit height={400} data={sourceData}>
                <Tooltip />
                <Axis />
                <Legend />
                <Bar
                  position="locatefit*estadistica"
                  color="name"
                  adjust={[{ type: "dodge", marginRatio: 1 / 32 }]}
                />
              </Chart>
            );
          }}
        </Query>
      </div>
    );
  }
}

export default withApollo(Estadisticas);
