import React, { useState } from "react";
import { Icon, Image, Input } from "semantic-ui-react";
import { Link, withRouter } from "react-router-dom";
import UserImage from "../../Assets/images/user.png";
import { Badge } from "antd";
import { query, mutations } from "../../GraphQL";
import { useQuery, useMutation } from "react-apollo";
import Switch from "react-switch";
import { toast } from "react-toastify";

import "./topbar.scss";

function Topbar(props) {
  //const [search, setsearch] = useState("");
  const { history, res } = props;
  const [open, setOpen] = useState(res.open);
  const [actualizarRestaurant] = useMutation(mutations.ACTUALIZAR_RESTAURANT);

  const { data } = useQuery(query.GET_NOTIFICATION, {
    variables: { Id: localStorage.getItem("id") },
  });

  const goBack = () => {
    history.goBack();
  };

  /* const onsearch = (e) => {
    setsearch(e.target.value);
  }; */

  const datos =
    data && data.getNotifications && data.getNotifications
      ? data.getNotifications.notifications
      : [];

  const saveInfo = () => {
    const input = {
      _id: res._id,
      open: res.open ? false : true,
    };
    actualizarRestaurant({ variables: { input: input } })
      .then((res) => {
        if (res.data.actualizarRestaurant.success) {
          toast.success("El restaurante ahora esta marcado como cerrado", {
            position: "top-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        } else {
          toast.error(res.data.actualizarRestaurant.messages, {
            position: "top-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
      })
      .then();
  };

  const handleChange = (checked) => {
    setOpen(checked);
    saveInfo();
  };

  return (
    <div className="top-bar">
      <div className="top-bar__left">
        <Icon
          name="angle left"
          onClick={goBack}
          style={{ marginBottom: 10, marginRight: 30 }}
        />
        <label>
          <Switch
            onChange={handleChange}
            checked={open}
            offColor="#3c3c3c"
            onColor="#ffcb40"
          />
          <span style={{ marginLeft: 20 }}>{open ? "Abierto" : "Cerrado"}</span>
        </label>
      </div>
      <Input
        fluid
        icon="search"
        placeholder="Buscar en foody..."
        className="search"
        loading={false}
        onClick={() => history.push("/search")}
        onChange={onsearch}
        size="large"
      />
      <div className="top-bar__right">
        <Badge count={datos.length} overflowCount={9}>
          <Icon
            name="bell outline"
            onClick={() => history.push("/notification")}
          />
        </Badge>
        <Link to="/restaurant">
          <Image src={UserImage} />
        </Link>
      </div>
    </div>
  );
}

export default withRouter(Topbar);
