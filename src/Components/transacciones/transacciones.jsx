import React from "react";
import { query } from "../../GraphQL";
import { Query } from "react-apollo";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import moment from "moment";

import "./transacciones.scss";

export default function Pago(props) {
  const { res } = props;

  return (
    <>
      <div className="restaurant">
        <h3 style={{ marginBottom: 50 }}>Transacciones en Foody</h3>
        <Query query={query.GET_TRANSACTION} variables={{ id: res._id }}>
          {(response) => {
            if (response.loading) {
              return (
                <>
                  <SkeletonTheme color="#202020" highlightColor="#444">
                    <div style={{ marginTop: 35 }}>
                      <Skeleton height={40} width="100%" />
                    </div>
                  </SkeletonTheme>
                  <SkeletonTheme color="#202020" highlightColor="#444">
                    <div style={{ marginTop: 35 }}>
                      <Skeleton height={40} width="100%" />
                    </div>
                  </SkeletonTheme>
                  <SkeletonTheme color="#202020" highlightColor="#444">
                    <div style={{ marginTop: 35 }}>
                      <Skeleton height={40} width="100%" />
                    </div>
                  </SkeletonTheme>
                  <SkeletonTheme color="#202020" highlightColor="#444">
                    <div style={{ marginTop: 35 }}>
                      <Skeleton height={40} width="100%" />
                    </div>
                  </SkeletonTheme>
                  <SkeletonTheme color="#202020" highlightColor="#444">
                    <div style={{ marginTop: 35 }}>
                      <Skeleton height={40} width="100%" />
                    </div>
                  </SkeletonTheme>
                  <SkeletonTheme color="#202020" highlightColor="#444">
                    <div style={{ marginTop: 35 }}>
                      <Skeleton height={40} width="100%" />
                    </div>
                  </SkeletonTheme>
                </>
              );
            }
            if (response) {
              const respuesta =
                response && response.data && response.data.getTransaction
                  ? response.data.getTransaction.list
                  : [];
              response.refetch();
              return (
                <>
                  {respuesta.length > 0 ? (
                    <>
                      {respuesta.map((d, i) => {
                        return (
                          <>
                            <div key={i} className="grids_transacciones">
                              <p style={{ color: "#95ca3e" }}>{d.estado}</p>
                              <p>{moment(Number(d.fecha)).format("lll")}</p>
                              <p>{d.total} €</p>
                            </div>
                            <div className="separator" />
                          </>
                        );
                      })}
                    </>
                  ) : (
                    <div style={{ textAlign: "center", paddingBottom: 30 }}>
                      <img
                        src="https://api.foodyapp.es/assets/images/wallet.png"
                        alt="crear menu"
                        style={{ width: 250 }}
                      />
                      <h4 style={{ color: "white" }}>
                        No tienes transacciones de Foody.
                      </h4>
                    </div>
                  )}
                </>
              );
            }
          }}
        </Query>
      </div>
    </>
  );
}
