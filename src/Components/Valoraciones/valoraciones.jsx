import React from "react";
import { withRouter } from "react-router-dom";
import { Icon } from "semantic-ui-react";
import { query } from "../../GraphQL";
import { useQuery } from "react-apollo";
import { IMAGES_PATH } from "../../config";
import moment from "moment";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import "./valoraciones.scss";

function Valoraciones(props) {
  const { res } = props;
  const { data, loading } = useQuery(query.GET_VALORACION, {
    variables: { restaurant: res._id },
  });

  if (loading) {
    return (
      <div className="valoraciones">
        <SkeletonTheme color="#202020" highlightColor="#444">
          <div style={{ marginTop: 20 }}>
            <Skeleton height={120} width="100%" />
          </div>
        </SkeletonTheme>
        <SkeletonTheme color="#202020" highlightColor="#444">
          <div style={{ marginTop: 20 }}>
            <Skeleton height={120} width="100%" />
          </div>
        </SkeletonTheme>
        <SkeletonTheme color="#202020" highlightColor="#444">
          <div style={{ marginTop: 20 }}>
            <Skeleton height={120} width="100%" />
          </div>
        </SkeletonTheme>
        <SkeletonTheme color="#202020" highlightColor="#444">
          <div style={{ marginTop: 20 }}>
            <Skeleton height={120} width="100%" />
          </div>
        </SkeletonTheme>
        <SkeletonTheme color="#202020" highlightColor="#444">
          <div style={{ marginTop: 20 }}>
            <Skeleton height={120} width="100%" />
          </div>
        </SkeletonTheme>
        <SkeletonTheme color="#202020" highlightColor="#444">
          <div style={{ marginTop: 20 }}>
            <Skeleton height={120} width="100%" />
          </div>
        </SkeletonTheme>
      </div>
    );
  }

  const datos = data.getValoraciones.data;

  let rating = { 1: 0, 2: 0, 3: 0, 4: 0, 5: 0 };
  datos.forEach((start) => {
    if (start.value === 1) rating["1"] += 1;
    else if (start.value === 2) rating["2"] += 1;
    else if (start.value === 3) rating["3"] += 1;
    else if (start.value === 4) rating["4"] += 1;
    else if (start.value === 5) rating["5"] += 1;
  });

  const ar =
    (5 * rating["5"] +
      4 * rating["4"] +
      3 * rating["3"] +
      2 * rating["2"] +
      1 * rating["1"]) /
    datos.length;
  let averageRating = 0;
  if (datos.length) {
    averageRating = Number(ar.toFixed(1));
  }

  return (
    <div className="valoraciones">
      <h3 className="titles" style={{ paddingTop: 30 }}>
        Opiniones de cliente
      </h3>
      <h1 className="titlesh1">
        {averageRating} /5 de ({datos.length}) de valoraciones
      </h1>
      {datos.length > 0 ? (
        <>
          {datos.map((d, i) => {
            return (
              <div className="card__container" key={i}>
                <div className="card__header" style={{ display: "flex" }}>
                  <img
                    src={IMAGES_PATH + d.Usuario.avatar}
                    alt=""
                    className="avatar"
                  />
                  <div style={{ marginLeft: 15 }}>
                    <h4 className="name">
                      {d.Usuario.name} {d.Usuario.lastName}
                    </h4>
                    <p className="date">{moment(d.created_at).format("lll")}</p>
                  </div>

                  <div style={{ marginLeft: "auto", display: "flex" }}>
                    <Icon name="star" style={{ color: "#ffa500" }} />
                    <p style={{ paddingTop: 3 }}>{d.value}</p>
                  </div>
                </div>
                <p className="comments">{d.comment}</p>
              </div>
            );
          })}
        </>
      ) : (
        <div style={{ textAlign: "center", paddingBottom: 30 }}>
          <img
            src="https://api.foodyapp.es/assets/images/nocoment.png"
            alt="crear menu"
            style={{ width: 250 }}
          />
          <h3 style={{ color: "white" }}>
            Aún no has creado un menú para tu restaurante.
          </h3>
        </div>
      )}
    </div>
  );
}

export default withRouter(Valoraciones);
