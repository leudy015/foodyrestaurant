import React, { useState } from "react";
import { Button, Form, Input, Icon } from "semantic-ui-react";
import { validateEmail } from "../../../Utils/validations";
import { toast } from "react-toastify";
import { useMutation } from "react-apollo";
import { mutations } from "../../../GraphQL/index";
import "./LoginForm.scss";

const LoginForm = (props) => {
  const { setSelectedForm } = props;
  const [dataForm, setDataForm] = useState(SetElements());
  const [formError, setFormError] = useState({});
  const [Loading, setLoanding] = useState(false);
  const [showpassword, setShowpassword] = useState(false);
  const [autenticarRestaurant] = useMutation(mutations.AUTENTICAR_RESTAURANT);

  const showPass = () => {
    setShowpassword(!showpassword);
  };

  const onChage = (e) => {
    setDataForm({
      ...dataForm,
      [e.target.name]: e.target.value,
    });
  };

  const onSubmit = async () => {
    setFormError({});
    let errors = {};
    let formOk = true;

    if (!validateEmail(dataForm.email)) {
      errors.email = true;
      formOk = false;
    }

    if (!dataForm.password) {
      errors.password = true;
      formOk = false;
    }

    setFormError(errors);

    if (formOk) {
      setLoanding(true);
      autenticarRestaurant({
        variables: { email: dataForm.email, password: dataForm.password },
      })
        .then((res) => {
          const datas =
            res && res.data && res.data.autenticarRestaurant
              ? res.data.autenticarRestaurant
              : "";
          localStorage.setItem("token", datas.data ? datas.data.token : "");
          localStorage.setItem("id", datas.data ? datas.data.id : "");
          const errores = res && res.data ? res.data.autenticarRestaurant : "";

          if (errores.success) {
            window.location.reload();
          }

          if (!errores.success) {
            toast.error(errores.message, {
              position: "top-center",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
          }
        })
        .catch((err) => {})
        .finally(() => {
          setLoanding(false);
        });
    }
  };

  return (
    <div className="login-form">
      <h2>Bienvenido a Foody</h2>
      <Form onSubmit={onSubmit}>
        <Form.Field>
          <Input
            type="text"
            name="email"
            placeholder="Email"
            icon="mail outline"
            onChange={onChage}
            error={formError.email}
          />
          {formError.email && (
            <span className="error-text">
              Por favor introduce un email válido
            </span>
          )}
        </Form.Field>
        <Form.Field>
          <Input
            type={showpassword ? "text" : "password"}
            name="password"
            placeholder="Contraseña"
            icon={
              showpassword ? (
                <Icon
                  name="eye slash outline"
                  link
                  onClick={() => showPass()}
                />
              ) : (
                <Icon name="eye" link onClick={() => showPass()} />
              )
            }
            onChange={onChage}
            error={formError.password}
          />
          {formError.password && (
            <span className="error-text">
              Ingresa un acontraseña valida por favor
            </span>
          )}
        </Form.Field>
        <Button type="submit" loading={Loading}>
          Iniciar sesión
        </Button>
      </Form>

      <div className="login-form__options">
        <p onClick={() => setSelectedForm(null)}>Volver al inicio</p>
        <p>
          ¿La olvidaste? {""}
          <span onClick={() => setSelectedForm("recovery")}>
            Recuperar contraseña
          </span>
        </p>
      </div>
    </div>
  );
};

export default LoginForm;

function SetElements() {
  return {
    email: "",
    password: "",
  };
}
