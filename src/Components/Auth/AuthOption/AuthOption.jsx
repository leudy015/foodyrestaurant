import React from "react";
import { Button } from "semantic-ui-react";

import "./AuthOption.scss";

const AuthOption = (props) => {
  const { setSelectedForm } = props;

  return (
    <div className="auth-option">
      <h2>Bienvenido a Foody for Restaurants</h2>
      <Button className="login" onClick={() => setSelectedForm("login")}>
        Iniciar sesión
      </Button>
      <Button className="register" onClick={() => setSelectedForm("register")}>
        Contacta con nosotros
      </Button>
    </div>
  );
};

export default AuthOption;
