import React, { useState } from "react";
import { Button, Form, Input } from "semantic-ui-react";
import { validateEmail } from "../../../Utils/validations";
import { toast } from "react-toastify";
import { LOCAL_API_URL } from "../../../config";

import "./Recovery.scss";

const Recovery = (props) => {
  const { setSelectedForm } = props;
  const [dataForm, setDataForm] = useState(SetElements());
  const [formError, setFormError] = useState({});
  const [Loading, setLoanding] = useState(false);

  const onChage = (e) => {
    setDataForm({
      ...dataForm,
      [e.target.name]: e.target.value,
    });
  };

  const onSubmit = async () => {
    setFormError({});
    let errors = {};
    let formOk = true;

    if (!validateEmail(dataForm.email)) {
      errors.email = true;
      formOk = false;
    }

    setFormError(errors);

    if (formOk) {
      setLoanding(true);
      let res = await fetch(
        `${LOCAL_API_URL}/forgotpassword-restaurant?email=${dataForm.email}`
      );
      if (res) {
        const user = await res.json();
        if (!user.success) {
          setLoanding(false);
          toast.error(user.message, {
            position: "top-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        } else {
          toast.success("Email enviado con éxito", {
            position: "top-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          setLoanding(false);
          setSelectedForm("login");
        }
      } else {
        toast.error("Ups hubo un error interno con el sistema", {
          position: "top-center",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        setLoanding(false);
      }
    }
  };

  return (
    <div className="recovery-form">
      <h2>Recuperar contraseña</h2>
      <p>
        Escribe tu email y te enviaremos un enlace para cambiar tu contraseña
      </p>
      <Form onSubmit={onSubmit}>
        <Form.Field>
          <Input
            type="text"
            name="email"
            placeholder="Email"
            icon="mail outline"
            onChange={onChage}
            error={formError.email}
          />
          {formError.email && (
            <span className="error-text">
              Por favor introduce un email válido
            </span>
          )}
        </Form.Field>
        <Button type="submit" loading={Loading}>
          Recuperar contraseña
        </Button>
      </Form>

      <div className="recovery-form__options">
        <p onClick={() => setSelectedForm(null)}>Volver al inicio</p>
        <p>
          ¿Tienes una cuenta? {""}
          <span onClick={() => setSelectedForm("login")}>Iniciar sesión</span>
        </p>
      </div>
    </div>
  );
};

export default Recovery;

function SetElements() {
  return {
    email: "",
  };
}
