import React, { useState } from "react";
import { Button, Form, Input } from "semantic-ui-react";
import { validateEmail } from "../../../Utils/validations";
import { LOCAL_API_URL } from "../../../config";
import { toast } from "react-toastify";

import "./register.scss";

const RegisterForm = (props) => {
  const { setSelectedForm } = props;
  const [dataForm, setDataForm] = useState(SetElements());
  const [formError, setFormError] = useState({});
  const [Loading, setLoanding] = useState(false);

  const onChage = (e) => {
    setDataForm({
      ...dataForm,
      [e.target.name]: e.target.value,
    });
  };

  const onSubmit = async () => {
    setFormError({});
    let errors = {};
    let formOk = true;

    if (!validateEmail(dataForm.email)) {
      errors.email = true;
      formOk = false;
    }

    if (!dataForm.name) {
      errors.name = true;
      formOk = false;
    }

    if (!dataForm.phone) {
      errors.phone = true;
      formOk = false;
    }

    if (dataForm.phone.length < 9) {
      errors.phone = true;
      formOk = false;
    }

    setFormError(errors);

    if (formOk) {
      setLoanding(true);
      await fetch(`${LOCAL_API_URL}/contact-for-restaurant`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(dataForm),
      })
        .then((res) => {})
        .catch((err) => {
          toast.error(
            "Error al enviar tu solicitud vuelve a intentarlo por favor",
            {
              position: "top-center",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            }
          );
        })
        .finally(() => {
          toast.success(
            "Datos enviado con éxito no podremos en contacto contigo lo antes posible",
            {
              position: "top-center",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            }
          );
          setLoanding(false);
          setSelectedForm(null);
        });
    }
  };

  return (
    <div className="register-form">
      <h2>¿Aún no eres parte de Foody? contacta con nosotros para unirte</h2>
      <Form onSubmit={onSubmit}>
        <Form.Field>
          <Input
            type="text"
            name="name"
            placeholder="Nombre"
            icon="user outline"
            onChange={onChage}
            error={formError.name}
          />
          {formError.name && (
            <span className="error-text">Por favor introduce tu nombre</span>
          )}
        </Form.Field>
        <Form.Field>
          <Input
            type="text"
            name="email"
            placeholder="Email"
            icon="mail outline"
            onChange={onChage}
            error={formError.email}
          />
          {formError.email && (
            <span className="error-text">
              Por favor introduce un email válido
            </span>
          )}
        </Form.Field>
        <Form.Field>
          <Input
            type="number"
            name="phone"
            placeholder="Teléfono"
            icon="phone"
            onChange={onChage}
            error={formError.phone}
          />
          {formError.phone && (
            <span className="error-text">
              Por favor introduce nu móvil válido
            </span>
          )}
        </Form.Field>
        <Button type="submit" loading={Loading}>
          Contactar
        </Button>
      </Form>

      <div className="register-form__options">
        <p onClick={() => setSelectedForm(null)}>Volver al inicio</p>
        <p>
          ¿Ya tienes una cuenta? {""}
          <span onClick={() => setSelectedForm("login")}>Iniciar sesión</span>
        </p>
      </div>
    </div>
  );
};

export default RegisterForm;

function SetElements() {
  return {
    name: "",
    email: "",
    phone: "",
    restaurant: "",
  };
}
