import React, { useState } from "react";
import {
  Grid,
  Input,
  Button,
  Select,
  Checkbox,
  TextArea,
  Icon,
  Popup,
} from "semantic-ui-react";
import { mutations, query } from "../../GraphQL";
import { useMutation, Query, Mutation } from "react-apollo";
import { toast } from "react-toastify";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import { Upload } from "antd";
import { IMAGES_PATH } from "../../config";
import Modals from "../../Components/Modal/ModalAcompanante";
import { withRouter } from "react-router-dom";

import "./menu.scss";

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
}

function MenuRes(props) {
  const [imagen, setImagen] = useState(null);
  const [option, setOption] = useState([]);
  const [plato, setplato] = useState(Setplatos());
  const [menu, setMenu] = useState("");
  const [popular, setPopular] = useState(false);
  const [oferta, setOferta] = useState(false);
  const [news, setNews] = useState(false);
  const [loadinPlato, setloadinPlato] = useState(false);
  const [loadinMenu, setloadinMenu] = useState(false);
  const [createPlato, setCreatePlato] = useState(setMenudata());
  const [showModal, setshowModal] = useState(false);
  const [contentModal, setcontentModal] = useState(null);
  const [size, setsize] = useState("tiny");
  const [titless, settitless] = useState("");
  const [imaesmodal, setimaesmodal] = useState("");
  const [createMenu] = useMutation(mutations.CREAR_MENU);
  const [createPlatos] = useMutation(mutations.CREAR_PLATO);
  const [eliminarMenu] = useMutation(mutations.ELIMINAR_MENU);
  const [eliminarPlato] = useMutation(mutations.ELIMINAR_PLATO);
  const [eliminarComplemento] = useMutation(mutations.ELIMINAR_COMPLEMENTO);

  const { data, history } = props;

  let opciones = [
    {
      key: "",
      text: "",
      value: "",
    },
  ];

  opciones = [
    option &&
      option.map((r) => {
        return {
          key: r._id,
          text: r.title,
          value: r._id,
        };
      }),
  ];

  const uploadButton = (
    <div className="ant-upload-text">Añadir foto del plato</div>
  );

  const onChageFormDataPlato = (e) => {
    setplato({
      ...plato,
      [e.target.name]: e.target.value,
    });
  };

  const onChageFormDataMenu = (e) => {
    setCreatePlato({
      ...createPlato,
      [e.target.name]: e.target.value,
    });
  };

  const handleChange = (e, { value }) => setMenu(value);
  const handleOferta = (e, { checked }) => setOferta(checked);
  const handlePopular = (e, { checked }) => setPopular(checked);
  const handleNews = (e, { checked }) => setNews(checked);

  const anadirPlato = () => {
    setloadinPlato(true);
    const input = {
      title: plato.title,
      ingredientes: plato.ingredientes,
      price: `${plato.price}.${plato.centavo}`,
      restaurant: data._id,
      imagen: `${IMAGES_PATH}${imagen}`,
      menu: menu,
      oferta: oferta,
      popular: popular,
      news: news,
    };

    if (input.title === "" && input.price === "" && input.imagen === "") {
      alert("Debes añadir un titulo para continuar");
    } else {
      createPlatos({ variables: { input: input } })
        .then((res) => {
          if (res.data.createPlatos.success) {
            toast.success("Menú añadido con éxito", {
              position: "top-center",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
          } else {
            toast.error(
              "Hubo un error con su solicitud vulve a intentarlo por favor",
              {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
              }
            );
          }
        })
        .catch((error) => {
          toast.error("Debes completar todo los campos para continuar", {
            position: "top-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        })
        .finally(() => setloadinPlato(false));
    }
  };

  const anadirMenu = () => {
    setloadinMenu(true);
    const input = {
      title: createPlato.title,
      subtitle: createPlato.subtitle,
      restaurant: data._id,
    };

    if (createPlato.title === "") {
      alert("Debes añadir un titulo para continuar");
    } else {
      createMenu({ variables: { input: input } })
        .then((res) => {
          if (res.data.createMenu.success) {
            toast.success("Menú añadido con éxito", {
              position: "top-center",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
          } else {
            toast.error(
              "Hubo un error con su solicitud vulve a intentarlo por favor",
              {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
              }
            );
          }
        })
        .catch((error) => error)
        .finally(() => setloadinMenu(false));
    }
  };

  function Modalcontent(id) {
    return (
      <Query query={query.GET_ACOMPANANTE} variables={{ id: id }}>
        {(response) => {
          if (response.loading) {
            return null;
          }
          if (response) {
            const respuesta =
              response && response.data && response.data.getAcompanante
                ? response.data.getAcompanante.list
                : [];
            response.refetch();
            return (
              <>
                {respuesta.length === 0 ? (
                  <div className="nocontenido">
                    <img
                      src="https://api.foodyapp.es/assets/images/nodata.png"
                      alt="crear menu"
                      className="nocontenimage"
                    />
                    <h3>Aún no hay opciones para este plato.</h3>
                  </div>
                ) : (
                  <div className="content-modal">
                    {respuesta.map((d, i) => {
                      return (
                        <div>
                          <div className="acompanante-header" key={i}>
                            <h3>{d.name}</h3>
                            <Popup
                              content="Eliminar lista de opciones"
                              trigger={
                                <Icon
                                  name="trash"
                                  link
                                  onClick={() => deleteacompanante(d.id)}
                                  style={{
                                    marginLeft: "auto",
                                    marginRight: 20,
                                    paddingTop: 20,
                                    color: "#f5365c",
                                  }}
                                />
                              }
                            />
                          </div>
                          {d.children.map((f, i) => {
                            return (
                              <div key={i}>
                                <p>
                                  {f.name} {f.price ? `(+ ${f.price} €)` : null}
                                </p>
                                <div className="separator" />
                              </div>
                            );
                          })}
                        </div>
                      );
                    })}
                  </div>
                )}
              </>
            );
          }
        }}
      </Query>
    );
  }

  const handleModal = (c) => {
    settitless(c.title);
    setimaesmodal(c.imagen);
    setcontentModal(Modalcontent(c._id));
    setshowModal(true);
    setsize("tiny");
  };

  const deletemenu = (id) => {
    eliminarMenu({ variables: { id } })
      .then((res) => {
        if (res.data.eliminarMenu.success) {
          toast.success("Menú eliminado con éxito", {
            position: "top-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        } else {
          toast.error(
            "Hubo un error con su solicitud vulve a intentarlo por favor",
            {
              position: "top-center",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            }
          );
        }
      })
      .catch(() => {
        toast.error(
          "Hubo un error con su solicitud vulve a intentarlo por favor",
          {
            position: "top-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          }
        );
      });
  };

  const deleteplato = (id) => {
    eliminarPlato({ variables: { id } })
      .then((res) => {
        if (res.data.eliminarPlato.success) {
          toast.success("Plato eliminado con éxito", {
            position: "top-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        } else {
          toast.error(
            "Hubo un error con su solicitud vulve a intentarlo por favor",
            {
              position: "top-center",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            }
          );
        }
      })
      .catch(() => {
        toast.error(
          "Hubo un error con su solicitud vulve a intentarlo por favor",
          {
            position: "top-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          }
        );
      });
  };

  const deleteacompanante = (id) => {
    eliminarComplemento({ variables: { id } })
      .then((res) => {
        if (res.data.eliminarComplemento.success) {
          toast.success("Opciones eliminada con éxito", {
            position: "top-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        } else {
          toast.error(
            "Hubo un error con su solicitud vulve a intentarlo por favor",
            {
              position: "top-center",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            }
          );
        }
      })
      .catch(() => {
        toast.error(
          "Hubo un error con su solicitud vulve a intentarlo por favor",
          {
            position: "top-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          }
        );
      });
  };

  return (
    <>
      <div className="container-menu">
        <Grid className="container-menu__layout">
          <Grid.Row>
            <Grid.Column width={8}>
              <h3>Crear menú</h3>
              <div>
                <Input
                  placeholder="Titulo   (Ejm Las Ensaladas)"
                  name="title"
                  onChange={onChageFormDataMenu}
                />
                <Input
                  placeholder="Descripción   (Ejm La más aplia variedad de ensaladas)"
                  name="subtitle"
                  onChange={onChageFormDataMenu}
                />
                <Button onClick={() => anadirMenu()} loading={loadinMenu}>
                  Crear menú
                </Button>
              </div>

              <h3 style={{ marginTop: 70 }}>Crear plato</h3>
              <div>
                <Mutation mutation={mutations.UPLOAD_FILE}>
                  {(singleUpload) => (
                    <Upload
                      name="imagen"
                      listType="picture-card"
                      className="avatar-uploader"
                      showUploadList={false}
                      customRequest={async (data) => {
                        let imgBlob = await getBase64(data.file);
                        singleUpload({ variables: { imgBlob } })
                          .then((res) => {
                            setImagen(
                              res && res.data && res.data.singleUpload
                                ? res.data.singleUpload.filename
                                : ""
                            );
                          })
                          .catch((error) => {});
                      }}
                    >
                      {imagen ? (
                        <img
                          src={IMAGES_PATH + imagen}
                          alt="avatar"
                          style={{ width: 180, height: 180 }}
                        />
                      ) : null}

                      {!imagen ? uploadButton : null}
                    </Upload>
                  )}
                </Mutation>
                <Input
                  placeholder="Titulo   (Ejm Alitas de pollo)"
                  name="title"
                  onChange={onChageFormDataPlato}
                />
                <TextArea
                  placeholder="Ingredientes"
                  name="ingredientes"
                  onChange={onChageFormDataPlato}
                />
                <Input
                  placeholder="Precio Ejemplo (12)"
                  name="price"
                  style={{ width: 200, marginRight: 15 }}
                  onChange={onChageFormDataPlato}
                />
                <Input
                  placeholder="Centimos Ejemplo (95)"
                  name="centavo"
                  style={{ width: 200 }}
                  onChange={onChageFormDataPlato}
                />

                <Select
                  options={opciones[0]}
                  placeholder="Selecciona un menú"
                  onChange={handleChange}
                />

                <Checkbox
                  name="oferta"
                  label="Marcar en oferta"
                  onChange={handleOferta}
                />
                <Checkbox
                  onChange={handlePopular}
                  name="popular"
                  label="Marcar como popular"
                />
                <Checkbox
                  name="news"
                  label="Marcar como nuevo"
                  onChange={handleNews}
                />

                <Button onClick={() => anadirPlato()} loading={loadinPlato}>
                  Crear plato
                </Button>
              </div>
            </Grid.Column>
            <Grid.Column width={8}>
              <Query query={query.GET_MENU} variables={{ id: data._id }}>
                {(response) => {
                  if (response.loading) {
                    return (
                      <>
                        <SkeletonTheme color="#202020" highlightColor="#444">
                          <div style={{ marginTop: 35 }}>
                            <Skeleton height={170} width="100%" />
                          </div>
                        </SkeletonTheme>
                        <SkeletonTheme color="#202020" highlightColor="#444">
                          <div style={{ marginTop: 35 }}>
                            <Skeleton height={170} width="100%" />
                          </div>
                        </SkeletonTheme>
                        <SkeletonTheme color="#202020" highlightColor="#444">
                          <div style={{ marginTop: 35 }}>
                            <Skeleton height={170} width="100%" />
                          </div>
                        </SkeletonTheme>
                        <SkeletonTheme color="#202020" highlightColor="#444">
                          <div style={{ marginTop: 35 }}>
                            <Skeleton height={170} width="100%" />
                          </div>
                        </SkeletonTheme>
                        <SkeletonTheme color="#202020" highlightColor="#444">
                          <div style={{ marginTop: 35 }}>
                            <Skeleton height={170} width="100%" />
                          </div>
                        </SkeletonTheme>
                        <SkeletonTheme color="#202020" highlightColor="#444">
                          <div style={{ marginTop: 35 }}>
                            <Skeleton height={170} width="100%" />
                          </div>
                        </SkeletonTheme>
                      </>
                    );
                  }
                  if (response) {
                    const respuesta =
                      response && response.data && response.data.getMenu
                        ? response.data.getMenu.list
                        : [];
                    response.refetch();
                    setOption(respuesta);
                    return (
                      <div>
                        <h3>Menú</h3>

                        {respuesta.length > 0 ? (
                          <>
                            {respuesta.map((da, i) => {
                              return (
                                <div key={i}>
                                  <div style={{ display: "flex" }}>
                                    <div style={{ marginBottom: 30 }}>
                                      <h4>{da.title}</h4>
                                      <p>{da.subtitle}</p>
                                    </div>
                                    <Popup
                                      content="Eliminar menú"
                                      trigger={
                                        <Icon
                                          name="trash"
                                          link
                                          onClick={() => deletemenu(da._id)}
                                          style={{
                                            marginLeft: "auto",
                                            marginRight: 20,
                                            paddingTop: 20,
                                            color: "#f5365c",
                                          }}
                                        />
                                      }
                                    />
                                  </div>
                                  {da.platos.map((c, i) => {
                                    return (
                                      <div className="mini-card" key={i}>
                                        <img src={c.imagen} alt={c.title} />
                                        <Popup
                                          content="Eliminar plato"
                                          trigger={
                                            <Icon
                                              name="trash"
                                              link
                                              onClick={() => deleteplato(c._id)}
                                              style={{
                                                marginLeft: "auto",
                                                marginRight: 20,
                                                color: "#f5365c",
                                              }}
                                            />
                                          }
                                        />
                                        <div>
                                          <div style={{ display: "flex" }}>
                                            <h3>{c.title}</h3>
                                            {c.news ? (
                                              <span className="new">Nuevo</span>
                                            ) : null}
                                          </div>
                                          <p>{c.ingredientes}</p>
                                          <div
                                            style={{
                                              display: "flex",
                                            }}
                                          >
                                            <p>{c.price}€</p>

                                            {c.popular ? (
                                              <p
                                                style={{
                                                  color: "#ffa500",
                                                  marginLeft: 15,
                                                }}
                                              >
                                                Popular
                                              </p>
                                            ) : null}

                                            {c.oferta ? (
                                              <p
                                                style={{
                                                  color: "#95ca3e",
                                                  marginLeft: 15,
                                                }}
                                              >
                                                Oferta
                                              </p>
                                            ) : null}
                                          </div>
                                          <div style={{ display: "flex" }}>
                                            <Button
                                              onClick={() => handleModal(c)}
                                            >
                                              Detalles
                                            </Button>
                                            <Button
                                              onClick={() =>
                                                history.push(
                                                  `detail-plato/${c._id}`
                                                )
                                              }
                                            >
                                              Opciones
                                            </Button>
                                          </div>
                                        </div>
                                      </div>
                                    );
                                  })}
                                </div>
                              );
                            })}
                          </>
                        ) : (
                          <div style={{ textAlign: "center" }}>
                            <img
                              src="https://api.foodyapp.es/assets/images/nodata.png"
                              alt="crear menu"
                              className="nocontenimage"
                            />
                            <h3>
                              Aún no has creado un menú para tu restaurante.
                            </h3>
                          </div>
                        )}
                      </div>
                    );
                  }
                }}
              </Query>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
      <Modals
        show={showModal}
        setshow={setshowModal}
        title={titless}
        imagen={imaesmodal}
        size={size}
      >
        {contentModal}
      </Modals>
    </>
  );
}

function Setplatos() {
  return {
    title: "",
    ingredientes: "",
    price: "",
    centavo: "",
  };
}

function setMenudata() {
  return {
    title: "",
    subtitle: "",
    restaurant: "",
  };
}

export default withRouter(MenuRes);
