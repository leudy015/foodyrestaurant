import React, { useState } from "react";
import { Grid, Input, Button } from "semantic-ui-react";
import { mutations, query } from "../../GraphQL";
import { useMutation, Query } from "react-apollo";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";

import "./pago.scss";

export default function Pago(props) {
  const { res } = props;
  const [pagoData, setPagoData] = useState(SetPago());
  const [loading, setLoading] = useState(false);
  const [loadingdelete, setLoadingdelete] = useState(false);
  const [eliminarPago] = useMutation(mutations.ELIMINAR_PAGO);
  const [crearPago] = useMutation(mutations.CREAR_PAGO);
  const [disable, setDisable] = useState(null);

  const onChagedata = (e) => {
    e.preventDefault();
    setPagoData({
      ...pagoData,
      [e.target.name]: e.target.value,
    });
  };

  const savePago = () => {
    setLoading(true);
    const input = {
      nombre: pagoData.nombre,
      iban: pagoData.iban,
      restaurantID: res._id,
    };

    crearPago({ variables: { input } })
      .then((res) => {})
      .catch((err) => err)
      .finally(() => setLoading(false));
  };

  const deletePago = (id, refetch) => {
    eliminarPago({ variables: { id } })
      .then(() => {
        refetch();
      })
      .catch((err) => err)
      .finally(() => setLoadingdelete(false));
  };

  return (
    <>
      <div className="restaurant">
        <Grid className="lrestaurant-layout">
          <Grid.Row>
            <Grid.Column width={8}>
              <h3 style={{ marginBottom: 50 }}>Añadir método de cobro</h3>
              <Input
                placeholder="Nombre completo"
                name="nombre"
                onChange={onChagedata}
                disabled={disable ? true : false}
              />
              <Input
                disabled={disable ? true : false}
                placeholder="Iban bancarío"
                name="iban"
                onChange={onChagedata}
              />

              <Button
                onClick={() => savePago()}
                loading={loading}
                disabled={disable ? true : false}
              >
                Añadir pago
              </Button>
            </Grid.Column>
            <Grid.Column width={8}>
              <>
                <h3 style={{ marginBottom: 50 }}>Mis métodos de pago</h3>
                <Query query={query.GET_PAGO} variables={{ id: res._id }}>
                  {(response) => {
                    if (response.loading) {
                      return (
                        <SkeletonTheme color="#202020" highlightColor="#444">
                          <div style={{ marginTop: 35 }}>
                            <Skeleton height={170} width="100%" />
                          </div>
                        </SkeletonTheme>
                      );
                    }
                    if (response) {
                      const respuesta =
                        response && response.data && response.data.getPago
                          ? response.data.getPago.data
                          : [];

                      response.refetch();
                      setDisable(respuesta);
                      return (
                        <>
                          {respuesta ? (
                            <div>
                              <Input
                                placeholder={respuesta.nombre}
                                onChange={onChagedata}
                                disabled={true}
                              />
                              <Input
                                disabled={true}
                                placeholder={respuesta.iban}
                                onChange={onChagedata}
                              />
                              <Button
                                onClick={() =>
                                  deletePago(respuesta._id, response.refetch)
                                }
                                loading={loadingdelete}
                                style={{ backgroundColor: "#f5365c" }}
                              >
                                Eliminar
                              </Button>
                            </div>
                          ) : (
                            <div
                              style={{ textAlign: "center", paddingBottom: 30 }}
                            >
                              <img
                                src="https://api.foodyapp.es/assets/images/wallet.png"
                                alt="crear menu"
                                style={{ width: 250 }}
                              />
                              <h4 style={{ color: "white" }}>
                                No tienes método de cobro añadido.
                              </h4>
                            </div>
                          )}
                        </>
                      );
                    }
                  }}
                </Query>
              </>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    </>
  );
}

function SetPago() {
  return {
    nombre: "",
    iban: "",
  };
}
