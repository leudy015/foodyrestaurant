import React, { useState } from "react";
import Card from "../../CardOrder";
import { DatePicker } from "antd";
import { Button } from "semantic-ui-react";
import { query } from "../../../GraphQL";
import { Query } from "react-apollo";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import moment from "moment";
import locate from "moment/locale/es";

import "./pedidos.scss";

const { RangePicker } = DatePicker;

const Order = (props) => {
  const [dateRange, setDateRange] = useState(null);

  const dateFormat = "MM/DD/YYYY"; // compatible con la fecha y hora del servidor para evitar conflictos de API
  const fromDate = "11/07/2019";
  const toDate = "12/04/2019";

  const onDateRangeChange = (dates, dateStrings) => {
    const fromDate = dateStrings[0];
    const toDate = dateStrings[1];
    setDateRange({ fromDate: fromDate, toDate: toDate });
  };

  const aplicarDateRange = () => {
    if (dateRange) {
      setDateRange(null);
    } else {
      setDateRange({ fromDate: fromDate, toDate: toDate });
    }
  };

  return (
    <div className="container-order">
      <h1 style={{ color: "white" }}>Todos los pedidos</h1>
      <div style={{ flexDirection: "row" }} className="date">
        <h4>Filtrar por fecha</h4>
        <RangePicker
          locale={locate}
          defaultValue={[
            moment(fromDate, dateFormat),
            moment(toDate, dateFormat),
          ]}
          format={dateFormat}
          onChange={onDateRangeChange}
          style={{ height: 50 }}
        />
        <Button
          onClick={() => aplicarDateRange()}
          style={{ marginBottom: 20, marginTop: 10, marginLeft: 5, height: 50 }}
          type="dashed"
        >
          {dateRange ? "Cancelar" : "Aplicar"}
        </Button>
      </div>
      <div style={{ marginTop: 30 }}>
        <Query
          query={query.GET_ORDEN}
          variables={{ id: props.data._id, dateRange: dateRange }}
        >
          {(response) => {
            if (response.loading) {
              return (
                <>
                  <SkeletonTheme color="#202020" highlightColor="#444">
                    <div>
                      <Skeleton height={250} width="100%" />
                    </div>
                  </SkeletonTheme>
                  <SkeletonTheme color="#202020" highlightColor="#444">
                    <div>
                      <Skeleton height={250} width="100%" />
                    </div>
                  </SkeletonTheme>
                  <SkeletonTheme color="#202020" highlightColor="#444">
                    <div>
                      <Skeleton height={250} width="100%" />
                    </div>
                  </SkeletonTheme>
                  <SkeletonTheme color="#202020" highlightColor="#444">
                    <div>
                      <Skeleton height={250} width="100%" />
                    </div>
                  </SkeletonTheme>
                  <SkeletonTheme color="#202020" highlightColor="#444">
                    <div>
                      <Skeleton height={250} width="100%" />
                    </div>
                  </SkeletonTheme>
                  <SkeletonTheme color="#202020" highlightColor="#444">
                    <div>
                      <Skeleton height={250} width="100%" />
                    </div>
                  </SkeletonTheme>
                  <SkeletonTheme color="#202020" highlightColor="#444">
                    <div>
                      <Skeleton height={250} width="100%" />
                    </div>
                  </SkeletonTheme>
                </>
              );
            }
            if (response) {
              const respuesta =
                response && response.data && response.data.getOrderByRestaurant
                  ? response.data.getOrderByRestaurant.list
                  : {};
              response.refetch();
              return (
                <>
                  {respuesta.length > 0 ? (
                    <Card
                      data={respuesta}
                      loading={response.loading}
                      refetch={response.refetch}
                    />
                  ) : (
                    <div style={{ textAlign: "center" }}>
                      <img
                        src="https://api.foodyapp.es/assets/images/nodata.png"
                        alt="crear menu"
                        className="nocontenimage"
                        style={{ width: 200 }}
                      />
                      <h3 style={{ color: "white" }}>
                        Aún no has recibido ningún pedido.
                      </h3>
                    </div>
                  )}
                </>
              );
            }
          }}
        </Query>
      </div>
    </div>
  );
};

export default Order;
