import React from "react";
import { Switch, Route } from "react-router-dom";

//Page
import Inicio from "../Containers/Inicio";
import Restaurant from "../Components/Restaurant";
import DetailsOrden from "../Components/detailsOrden";
import Menu from "../Components/MenuRestaurant";
import Add from "../Components/AddAcompanante";
import Valoraciones from "../Components/Valoraciones";
import Notification from "../Components/Notification";
import Pago from "../Components/Pago";
import Transacciones from "../Components/transacciones";
import Estadisticas from "../Components/estadisticas";
import Searc from "../Components/search";

function Router(props) {
  return (
    <Switch>
      <Route path="/" exact>
        <Inicio data={props.data} />
      </Route>
      <Route path="/restaurant" exact>
        <Restaurant data={props.data} />
      </Route>
      <Route path="/details-orden/:id" exact>
        <DetailsOrden />
      </Route>
      <Route path="/menu" exact>
        <Menu data={props.data} />
      </Route>
      <Route path="/detail-plato/:id" exact>
        <Add res={props.data} />
      </Route>
      <Route path="/opiniones" exact>
        <Valoraciones res={props.data} />
      </Route>
      <Route path="/notification" exact>
        <Notification res={props.data} />
      </Route>
      <Route path="/pago" exact>
        <Pago res={props.data} />
      </Route>
      <Route path="/transacciones" exact>
        <Transacciones res={props.data} />
      </Route>
      <Route path="/estadisticas" exact>
        <Estadisticas />
      </Route>
      <Route path="/search" exact>
        <Searc res={props.data} />
      </Route>
    </Switch>
  );
}

export default Router;
