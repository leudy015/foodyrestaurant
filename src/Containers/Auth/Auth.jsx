import React, { useState } from "react";
import AuthOption from "../../Components/Auth/AuthOption";
import LoginForm from "../../Components/Auth/LoginForm";
import Recovery from "../../Components/Auth/RecoveryPassword";
import RegisterForm from "../../Components/Auth/Register";
import Background from "../../Assets/images/back.jpg";
import Logo from "../../Assets/images/icon.png";
import "./Auth.scss";

const Auth = () => {
  const [seletecForm, setSelectedForm] = useState(null);

  const handleForm = () => {
    switch (seletecForm) {
      case "login":
        return <LoginForm setSelectedForm={setSelectedForm} />;
      case "register":
        return <RegisterForm setSelectedForm={setSelectedForm} />;
      case "recovery":
        return <Recovery setSelectedForm={setSelectedForm} />;
      default:
        return <AuthOption setSelectedForm={setSelectedForm} />;
    }
  };

  return (
    <div className="auth" style={{ backgroundImage: `url(${Background})` }}>
      <div className="auth__dark" />
      <div className="auth__box">
        <div className="auth__box-logo">
          <img src={Logo} alt="Foody Restaurant" />
        </div>
        {handleForm()}
      </div>
    </div>
  );
};

export default Auth;
