import React, { useState } from "react";
import Order from "../../Components/AllOrders/Pedidos";
import OrderNew from "../../Components/AllOrders/PedidosNew";
import OrderProcess from "../../Components/AllOrders/PedidosProccess";
import OrderListo from "../../Components/AllOrders/PedidosListos";

import "./inicio.scss";

export default function Inicio(props) {
  const [activeIndex, setActiveIndex] = useState("Nuevos pedidos");

  var content;

  function renderComponents() {
    switch (activeIndex) {
      case "Nuevos pedidos":
        content = <OrderNew data={props.data} />;
        break;

      case "Pedidos en proceso":
        content = <OrderProcess data={props.data} />;
        break;

      case "Listo para recoger":
        content = <OrderListo data={props.data} />;
        break;

      case "Todos los pedidos":
        content = <Order data={props.data} />;
        break;

      default:
        content = <OrderNew data={props.data} />;
        break;
    }

    return content;
  }

  return (
    <div className="containers__inicio">
      <div className="containers__inicio__item">
        <ul>
          <li>
            <button
              onClick={() => setActiveIndex("Nuevos pedidos")}
              className={activeIndex === "Nuevos pedidos" ? "active" : ""}
            >
              Nuevos pedidos
            </button>
          </li>
          <li>
            <button
              onClick={() => setActiveIndex("Pedidos en proceso")}
              className={activeIndex === "Pedidos en proceso" ? "active" : ""}
            >
              Pedidos en proceso
            </button>
          </li>
          <li>
            <button
              onClick={() => setActiveIndex("Listo para recoger")}
              className={activeIndex === "Listo para recoger" ? "active" : ""}
            >
              Listo para recoger
            </button>
          </li>
          <li>
            <button
              onClick={() => setActiveIndex("Todos los pedidos")}
              className={activeIndex === "Todos los pedidos" ? "active" : ""}
            >
              Todos los pedidos
            </button>
          </li>
        </ul>
      </div>
      <div>{renderComponents()}</div>
    </div>
  );
}
