import React, { useState, useEffect } from "react";
import Loggetlaout from "../../Components/Layout/Loggeding";
import { query } from "../../GraphQL";
import { Query } from "react-apollo";

import "./Home.scss";

const Home = () => {
  const [id, setId] = useState(null);

  useEffect(() => {
    const ids = localStorage.getItem("id");
    setId(ids);
  }, []);

  return (
    <div className="home-container">
      <Query query={query.RESTAURANT} variables={{ id: id }}>
        {(response) => {
          if (response.loading) {
            return null;
          }
          if (response) {
            const respuesta =
              response && response.data && response.data.getRestaurantForID
                ? response.data.getRestaurantForID.data
                : {};
            response.refetch();
            return <Loggetlaout data={respuesta} />;
          }
        }}
      </Query>
    </div>
  );
};

export default Home;
