export const LOCAL_API_URL = "https://api.foodyapp.es";
export const LOCAL_API_PATH = "/graphql";
export const IMAGES_PATH = `${LOCAL_API_URL}/assets/images/`;
