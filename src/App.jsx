import React, { useState, useEffect } from "react";
import Auth from "./Containers/Auth";
import Home from "./Containers/Home";
import { ToastContainer } from "react-toastify";

import "./App.scss";

function App() {
  const [user, setUser] = useState(null);
  const [loading, setLoading] = useState(true);

  const setingUser = () => {
    const user = localStorage.getItem("token");
    if (user) {
      setUser(user);
      setLoading(false);
    } else {
      setUser(null);
      setLoading(false);
    }
  };

  useEffect(() => {
    setingUser();
  }, [user]);

  if (loading) {
    return <h3>Loading App</h3>;
  }

  return (
    <>
      {!user ? (
        <Auth />
      ) : (
        <>
          <div
            style={{ textAlign: "center", paddingBottom: 50 }}
            className="aviso"
          >
            <img
              src="https://api.foodyapp.es/assets/images/nodata.png"
              alt="crear menu"
              style={{ width: 250 }}
            />
            <h3
              style={{
                color: "white",
                marginTop: -30,
                paddingLeft: 20,
                paddingRight: 20,
              }}
            >
              Está aplicación no está disponible en versión movíl utiliza un
              ordenador para abrirla.
            </h3>
          </div>
          <div className="init">
            <Home />
          </div>
        </>
      )}
      <ToastContainer
        position="top-center"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </>
  );
}

export default App;
